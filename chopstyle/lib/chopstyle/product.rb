require 'mongoid'
class Product
  include Mongoid::Document
  include Mongoid::Timestamps
  field :shopurl, type: String
  field :imageurl, type: String
  field :colors, type: String
  field :sizes, type: String
  field :description, type: String
  field :reference, type: String
  field :provider, type: String
  field :provider_rules, type: String
  field :moreinfo, type: String
  field :price, type: Integer, :default => 0

  

end

