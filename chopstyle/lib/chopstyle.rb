require_relative "chopstyle/version"
require_relative  'chopstyle/product'
require 'json'
require 'rubygems'
require 'rest_client'
require 'mongoid'
require 'hashie'

module Chopstyle

  def readfile
  	urls = []
  	line_num=0
	text=File.open('urls.txt').read
	text.gsub!(/\r\n?/, "\n")
	text.each_line do |line|
  		urls.push(line)
	end
	return urls
  end

  def execute 
  		Mongoid.load!("mongoid.yml", :development)

  		searchterm = "shopstyle"

  		Product.delete_all({:shopurl => ".*#{searchterm}.*"})

		urls = readfile()
		products = []

		urls.each { |url|

		response = RestClient.get(url)

		response_body = response.body.force_encoding("utf-8")

		document = JSON.parse(response_body)


		objects = document['products'].map { |pr| Product.new(description: pr['name'], price: (pr['price'])* 2000, shopurl: pr['clickUrl'], provider:pr['retailer']['name'], imageurl: pr['image']['sizes']['Original']['url'],moreinfo: pr['description'], sizes: pr['sizes'], colors: pr['colors']) }

    

		objects.each { |x| 

                begin  
  				   colors = JSON.parse(x.colors.gsub('\"', '"').gsub('=>',':').gsub('""','"'))
                   sizes = JSON.parse(x.sizes.gsub('\"', '"').gsub('=>',':').gsub('""','"'))
                   x.sizes = sizes.map {|v| v['name']}.join(",")
                   x.colors = colors.map {|v| v['name']}.join(",")
                   x.moreinfo.gsub('<ul>', '').gsub('<li>','')
                   if x.sizes.include? "}"
 						x.sizes = 'general'
                   end
                   if x.colors.include? "}"
 						x.colors = 'general'
                   end

				rescue Exception => e 
					colors = 'multicolor' 
					sizes = 'general' 
	  				puts e.message  
	  				puts e.backtrace.inspect  

				end  
  				   
                   
			       puts (x.description)
                   puts (x.price)
                   puts (x.provider) 
                   puts	(x.imageurl)
                   puts	(x.shopurl)
                   puts	(x.moreinfo)
                   puts (x.sizes)
                   puts (x.colors)
                   x.save
                   products.push(x)

         }
		}
		puts 'tamaño de nuevos productos -----'
		puts products.length
		Mongoid::Sessions.default.disconnect
		return products.length
  	end 
end


